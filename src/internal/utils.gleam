import gleam/result
import gleam/regex.{type Regex}
import gleam/list
import gleam/int
import gleam/iterator.{type Iterator, Done, Next}
import gleam/order.{type Order, Lt}


/// Returns true if the order is Less Than
pub fn lt(order: Order) -> Bool {
    case order {
        Lt -> True
        _ -> False
    }
}

/// Gives back all the Ok(element) in the list discrading all the errors
pub fn unwrap_all(input: List(Result(a, b))) -> List(a) {
    input 
    |> list.filter_map(fn (el) { el })
}

/// All elements of the list match regex
pub fn all_match_regex(re: Regex, list: List(String)) -> Bool {
    let check = fn (f) { regex.check(re, f) }
    case list {
        [_, ..] -> list |> list.all(check)

        [] -> False
    }
}

// NOTE: im not sure if this is basically clone of list.each? i just need to have
// a way to terminate the list.each and i thought this is teh way to do it
// (it's called apply cos im using it to apply scripts to db so idk dumb name
// mb should change)
/// Basically like list.each but it will terminate if error is encountered
pub fn apply_rec(iter: Iterator(String), cb: fn(String) -> Result(Nil, String)) {
    case iterator.step(iter) {
        Done -> Ok(Nil)
        Next(s, new_iter) -> {
            use _ <- result.try(cb(s))
            apply_rec(new_iter, cb)
        }
    }
}
        

/// Get the biggest value in the list of ints
pub fn max_in_list(list: List(Int)) -> Int {
    list
    |> list.fold(0, fn(prev, curr) { int.max(prev, curr) })
}
