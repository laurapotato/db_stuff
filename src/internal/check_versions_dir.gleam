import gleam/list
import gleam/bool
import gleam/regex
import gleam/result
import gleam/string
import gleam/int

import simplifile
import argv

import internal/utils
import internal/dbresult.{type DbResult}


/// Checks if all the files in directory follow the format
pub fn check_format(dir_path: String) -> DbResult(Bool) {
    use files <- result.try(get_all_files_dirless(dir_path))
    
    all_follow_format(files)
}

/// All entries follow this specific regex: \d+(\..*)?\.sql
pub fn all_follow_format(files: List(String)) -> DbResult(Bool) {
    use re <- result.try(
        regex.from_string("\\d+(\\..*)?\\.sql") 
        |> result.map_error(fn (e) { "cant parse regex! " <> e.error })
    ) 

    Ok(utils.all_match_regex(re, files))
}

/// This will return version whos number is the largest
pub fn get_latest_version(dir_path: String) -> DbResult(Int) {
    use version_nums <- result.try(get_all_versions(dir_path))

    Ok(utils.max_in_list(version_nums))
}


/// It gets all versions and extract the number part out of it.
pub fn get_all_versions(dir_path: String) -> DbResult(List(Int)) {
    use files <- result.try(get_all_files_dirless(dir_path))

    Ok(
        files
        |> list.filter_map(fn (el) { el |> string.split_once(".") } )
        |> list.map(fn (el) { el.0 })
        |> list.filter_map(fn (el) { int.parse(el) })
    )
}

/// Returns back the version if it is inside the version dir otherwise it will
/// return an Error (i know souhld be returnig Bool but im too lazy to fix it)
pub fn version_exists(num: Int, dir_path: String) -> DbResult(Int) {
    use versions <- result.try(get_all_versions(dir_path))

    let contains = versions |> list.contains(num)
    use <- bool.guard(contains, Error("cant find desired script"))

    Ok(num)
}

// NOTE: cring name
/// Parse the command line arguments and get the requested versions
pub fn get_desired_version(dir_path: String) -> DbResult(Int) {
    case argv.load().arguments {
        ["current"] -> get_latest_version(dir_path)
        [num] -> {
            use num <- result.try(
                int.parse(num) 
                |> result.replace_error("cant parse argument to a number")
            )
            version_exists(num, dir_path)
        }
        _ -> Error("bad args")
    }
}

/// A wrapper around simplifile.get_files() that converts the error to
/// something more appropriate in the context
pub fn get_all_files(dir_path: String) -> DbResult(List(String)) {
    dir_path
    |> simplifile.get_files()
    |> result.replace_error("cant open version directory")
}

// note: this is so ugly but u know it works for now
/// Same as `get_all_files` but teh dir_path is removed
pub fn get_all_files_dirless(dir_path: String) -> DbResult(List(String)) {
    dir_path
    |> simplifile.get_files()
    |> result.map(fn(files) { 
        files |> list.map(fn (path) { path |> string.replace(dir_path <> "/", "")
        })
    })
    |> result.replace_error("cant open version directory")
}
