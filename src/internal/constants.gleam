pub const env_file = ".env"

pub const db_host_var = "POSTGRES_HOST"
pub const db_name_var = "POSTGRES_DB"
pub const db_password_var = "POSTGRES_PASSWORD"
pub const db_user_var = "POSTGRES_USER"

pub const version_dir = "DB_STUFF_VERSION_DIR"
pub const separator = "~~~WOOO TIME TRAVEL~~~"
