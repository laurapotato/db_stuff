import gleam/dict.{type Dict}
import gleam/result
import gleam/string
import gleam/list
import simplifile

import internal/dbresult.{type DbResult}


pub type Envs = Dict(String, String)

pub fn get_envs(path: String) -> DbResult(Envs) {
    use file_content <- result.try(
        path 
        |> simplifile.read() 
        |> result.replace_error("cant read env file: " <> path )
    )

    let lines = file_content |> string.split("\n")

    let split_on_equals = fn (item) { string.split_once(item, "=") }
    let envs = lines 
        |> list.filter_map(split_on_equals)
        |> dict.from_list

    Ok(envs)
}

pub fn get_env(env: String, envs: Envs) {
    envs 
    |> dict.get(env) 
    |> result.replace_error("cannot get environment variable: " <> env)
}
