/// this is a simple  to do postgresql(only postgrsql) db migrations
/// all this does is:
/// 1. create current version table inside the database provided (if ther is
/// none)
///
/// 2. get all the versions between currently set version in that table and 
/// the latest found in the versions dir 
/// (files have to be named <number>.<anytihnguwant>.sql, 
/// if there is a file that is named weirdly the thing will panic)
///
/// 3. apply the sql scripts between the current verison and the desired one
/// (both down and up)
///
/// It doesn't check anything at all when it comes to dbs. If given scripts
// TODO: Mayebe put things into transaction

// NOTE: i didnt think even for a tiny lil second ok, poop codes ahead

import gleam/io
import gleam/dynamic
import gleam/pgo
import gleam/list
import gleam/iterator.{type Iterator}
import gleam/int
import gleam/bool
import gleam/dict.{type Dict}
import gleam/string
import gleam/result
import gleam/option.{type Option}


import internal/envs.{type Envs}
import simplifile

import internal/check_versions_dir as ver_dir
import internal/constants as consts
import internal/utils
import internal/dbresult.{type DbResult}

// Int for number String for filepath 
pub type Versions = Dict(Int, String)

/// You'd need this type for most of the important functions!!
pub type DbStuff {
    DbStuff(
        db_conn: pgo.Connection,
        versions: Versions,
        direction: Direction,
        desired_ver: Int,
        current_ver: Int
    )
}

/// A subset of pgo.Config that are determined by the user 
pub opaque type ConnConfig {
    ConnConfig(
        host: String,
        port: Int,
        database: String,
        user: String,
        password: Option(String),
    )
}

/// Which way migration is done
pub type Direction {
    Up
    Down
}

/// Create new DbStuff using variables from the .env file!!
/// You'd need this type for most of the important functions!!
/// If you dont want to use the .env file you could construct this 
/// type by hand.
pub fn new_dbstuff() ->  DbResult(DbStuff) {
    use envs <- result.try(envs.get_envs(consts.env_file))

    use dir_path <- result.try(envs.get_env(consts.version_dir, envs))

    use blah <- result.try(ver_dir.check_format(dir_path))
    use <- bool.guard(!blah, Error("scrips dont follow pathname format"))

    use user_conf <- result.try(new_conconfig(envs))
    let db_conn = connect_to_db(user_conf)

    use desired_ver <- result.try(ver_dir.get_desired_version(dir_path))
    use current_ver <- result.try(get_current_version(db_conn))

    use versions_num  <- result.try(ver_dir.get_all_versions(dir_path))
    use versions_path <- result.try(ver_dir.get_all_files(dir_path))

    let versions = list.zip(versions_num, versions_path)
    |> dict.from_list()

    let is_less = 
        int.compare(current_ver, desired_ver) 
        |> utils.lt()

    let direction = case is_less {
        True  -> Up
        False -> Down
    }

    Ok(DbStuff(
        db_conn: db_conn,
        versions: versions,
        direction: direction,
        desired_ver: desired_ver,
        current_ver: current_ver,
    ))
}

pub fn get_connection(db_stuff: DbStuff) -> pgo.Connection {
    db_stuff.db_conn
}

/// Create ConnConfig from the .env file
fn new_conconfig(envs: Envs) -> DbResult(ConnConfig) {
    use full_host <- result.try(envs.get_env(consts.db_host_var, envs))

    use #(host, port_str) <- result.try(
        full_host 
        |> string.split_once(":")
        |> result.replace_error("cant separate db host and port")
    )

    use port <- result.try(
        port_str |> int.parse() 
        |> result.replace_error("cant parse port to a number")
    )

    use user <- result.try(envs.get_env(consts.db_user_var, envs))

    use database <- result.try(envs.get_env(consts.db_name_var, envs))

    let password = envs.get_env(consts.db_password_var, envs) 
                   |> option.from_result()

    Ok(ConnConfig(
        host: host,
        port: port,
        database: database,
        user: user,
        password: password,
    ))
}

/// Executes all the provided sql scripts 
pub fn apply_scripts(scripts: List(String), db_stuff: DbStuff) -> DbResult(Nil) {
    let conn = db_stuff.db_conn

    use <- bool.guard(db_stuff.current_ver == db_stuff.desired_ver,
    Error("already at teh desired ver"))

    let start_transaction = "BEGIN; "
    use _ <- result.try(
        db_execute_discard(start_transaction, conn)
        |> result.replace_error("cant start transaction")
    )

    use _ <- result.try(
        scripts 
        |> iterator.from_list 
        |> apply_scripts_rec(conn)
        |> result.map_error(fn (er) {
            let script = er
            "some of your scripts are poop!!!!!!!!!!:\n" <> script
        })
    )

    let end_transaction = "COMMIT;"
    use _ <- result.try(
        db_execute_discard(end_transaction, conn)
        |> result.replace_error("cant commit transaction")
    )

    let update_version = "
        UPDATE __version_db_stuff SET 
        version = $1 
        WHERE id = 1;"
    use _ <- result.try(
        pgo.execute(update_version, conn, [pgo.int(db_stuff.desired_ver)], dynamic.dynamic) 
        |> result.replace_error("cant update the version")
    )


    Ok(Nil)
}


fn apply_scripts_rec(iter: Iterator(String), conn: pgo.Connection) 
    -> DbResult(Nil) {
    utils.apply_rec(iter, fn (script) {
        db_execute_discard(script, conn)
        |> result.map_error( fn (_) { 
            let rollback = "ROLLBACK TO __db_stuff;"
            let _ = db_execute_discard(rollback, conn)

            script
        })
    })
}

/// Run the sql statement with no input values and discard the result
fn db_execute_discard(sql_stmn: String, conn: pgo.Connection) {
    pgo.execute(sql_stmn, conn, [], dynamic.dynamic) |> result.replace(Nil)
}

/// This will get all the sql scripts that need to be applied to the db
/// it expects that scripts are formated in following way!!!!!:
///
///  <MIGRATE UP SQL_STMN>
///
///  ~~~WOOO TIME TRAVEL~~~
/// 
///  <MIGRATE DOWN SQL_STMN>
pub fn get_scripts(db_stuff: DbStuff) -> DbResult(List(String)) {
    let start = db_stuff.desired_ver
    let end   = db_stuff.current_ver

    let scripts = list.range(start, end)
        |> list.map(fn (el) { 
            extract_script(db_stuff.versions, db_stuff.direction, el) 
        })

    let has_error = scripts 
        |> list.find(fn (el) { result.is_error(el) })

    case has_error {
        Ok(Error(error)) -> Error(error)

        // no erro been found
        Error(Nil)
        | Ok(Ok(_)) -> Ok(utils.unwrap_all(scripts))

    }
}

/// Load scripts file content and get the `direciton` part of it
fn extract_script(versions: Versions, direction: Direction, el: Int) ->
    DbResult(String) {

    use script_file <- result.try(
        versions 
        |> dict.get(el)
        |> result.replace_error("cant find script number: " <> int.to_string(el))
    )
    use content <- result.try(
        simplifile.read(script_file)
        |> result.replace_error("cant read scriptfile" <> script_file)
    )

    get_script_split(content, direction)
}

/// Extract `direction` part of the script
pub fn get_script_split(script: String, direction: Direction) ->
    DbResult(String) {
    use #(up, down) <- result.try(
        script 
        |> string.split_once(consts.separator)
        |> result.replace_error("cant find my fancy separator")
    )

    case direction {
        Up -> Ok(up |> string.trim())
        Down -> Ok(down |> string.trim())
    }
}

fn get_current_version(conn: pgo.Connection) -> DbResult(Int) {
    let sql_stmn = "select version, id from __version_db_stuff;"

    let returned_type = dynamic.tuple2(
        dynamic.int,
        dynamic.dynamic
    )
    
    let version = pgo.execute(sql_stmn, conn, [], returned_type) 

    let rows = case version {
        Ok(ver) -> ver.rows
        Error(_) -> []
    }

    case rows {
        [#(v, _)] -> Ok(v)

        [] -> {
            io.print("no version in db found, creating one") 
            use _ <- result.try(create_version_table(conn))
            Ok(1)
        }

        [_, ..] -> Error("too many version tables found??? how did that happen")
    }
}

fn create_version_table(conn: pgo.Connection) -> DbResult(Nil) {
    let sql_stmn = "create table __version_db_stuff (id int, version int);"
    use _ <- result.try(
        pgo.execute(sql_stmn, conn, [], dynamic.dynamic)
        |> result.replace_error("cant create version table!")
    )

    let sql_stmn = "insert into __version_db_stuff (id, version) values ($1, $1);"
    use _ <- result.try(
        pgo.execute(sql_stmn, conn, [pgo.int(1)], dynamic.dynamic)
        |> result.replace_error("cant create verison table!")
    )
    
    Ok(Nil)
}

fn connect_to_db(user_conf: ConnConfig) -> pgo.Connection  {
    let config = pgo.Config (
        ..pgo.default_config(),
        host: user_conf.host,
        port: user_conf.port,
        user: user_conf.user,
        database: user_conf.database,
        password: user_conf.password,
    )

    io.debug(config)
    pgo.connect(config)
}

/// NOTE: im not sure if db is auto disconnected when garbage collector gets 
/// to the conneciton pool.  
//
/// Disconnect from the database! You should probably do that after you done
/// with your applying the scripts.
pub fn disconnect_db(db_stuff: DbStuff) {
    db_stuff.db_conn |> pgo.disconnect()
}

pub fn main() -> DbResult(Nil) {
    use db_stuff <- result.try(new_dbstuff())
    use scripts  <- result.try(get_scripts(db_stuff))
    use ok  <- result.try(apply_scripts(scripts, db_stuff))

    db_stuff |> disconnect_db()

    Ok(ok)
}
