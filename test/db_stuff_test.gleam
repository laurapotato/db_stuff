import gleam/result
import gleam/io
import gleam/dynamic
import gleam/pgo

import gleeunit
import gleeunit/should

import db_stuff.{Up, Down}
import internal/check_versions_dir

pub fn main() {
  gleeunit.main()
}

pub fn extract_script_test() {
    let blah = "script one\n ~~~WOOO TIME TRAVEL~~~ \nscript two"
    let assert Ok(up) = db_stuff.get_script_split(blah, Up)
    let assert Ok(down) = db_stuff.get_script_split(blah, Down)

    up
    |> should.equal("script one")
    down
    |> should.equal("script two")
    Ok("succ")
}

// realisticly all regexes should go thorugh hard fu,,, fu,zzing
pub fn all_follow_format_test() {
    let mock_files_fail = [
        "Blah", 
        "bluh", 
        "123sql", 
        "123.bshahsql", 
        "0.0.0"
    ]
    let mock_files_pass = [
        "123.blahu.sql", 
        "0.sql",
        "1.blah.sql",
        "2.blah.sql",
        "696969.6969699.sql", 
    ]

    check_versions_dir.all_follow_format(mock_files_pass)
    |> should.equal(Ok(True))

    check_versions_dir.all_follow_format(mock_files_fail)
    |> should.equal(Ok(False))
}

pub fn db_started_test() {
    let assert Ok(db_stuff_var) = db_stuff.new_dbstuff()
    let assert Ok(scripts ) = db_stuff.get_scripts(db_stuff_var)

    let assert Ok(_) = db_stuff.apply_scripts(scripts, db_stuff_var)

    let conn = db_stuff.get_connection(db_stuff_var)
    let sql_stmn = "insert into test_table values ($1, $2);"

    let insert_return_type = dynamic.tuple2(
        dynamic.int,
        dynamic.int,
    )

    let assert Ok(_) = 
        pgo.execute(
            sql_stmn, 
            conn, 
            [pgo.int(1), pgo.int(1)], 
            dynamic.dynamic
        ) 
        |> result.replace_error("test select 1 fail")

    let sql_stmn = "select * from test_table;" 
    let assert Ok(rows) = pgo.execute(sql_stmn, conn, [], insert_return_type) 
        |> result.replace_error("test insert 1 fail")

    rows.count 
    |> should.equal(1)

    io.debug(rows)
    
    let sql_stmn = "insert into blaher values ($1, $2);"

    let assert Ok(_) = 
        pgo.execute(
            sql_stmn, 
            conn, 
            [pgo.int(1), pgo.int(1)], 
            dynamic.dynamic
        ) 
        |> result.replace_error("test select 2 fail")

    let sql_stmn = "select * from blaher;" 
    let assert Ok(rows) = pgo.execute(sql_stmn, conn, [], insert_return_type) 
        |> result.replace_error("test insert 2 fail")

    rows.count 
    |> should.equal(1)
}
