simple thing to handle database migration. 

very tiny thing where there are not many migrations to be had

(cods very ugly too :D)

## usage

```console
$ db_stuff run current
```

or 

```console
$ db_stuff run <version num>
```

You have to put the database credentials and path to the version directory into .env  
file like so:  
```
POSTGRES_HOST=localhost:5432
POSTGRES_DB=mypressiousdata
POSTGRES_PASSWORD=test
POSTGRES_USER=tester

DB_STUFF_VERSION_DIR=db_versions
```

There is a container with db that you can start with
```console
$ docker-compose up test_startdb
```

the scripts themselves should have following format:

```
<migrate up script>

~~WOOO TIME TRAVEL~~~

<migrate down script>
```

## complie

This:
```console
$ gleam run -m gleescript
```
Will create static binary!!

Or using docker (untested):

```console
$ docker-compose run db_stuff_build
```

## !!!!warniningining!!!!

I have tested some amount but very little!! Should probably not run this  
on production database!!  

If you wish you can test it yourself by adding appropriate tests to ./test  
directory and running:  

```console
$ make gleam_test
```

This will create test database container and gleam container which will run the 
tests.
